# JS Library

A collection of useful js snippets and patterns.

## Index
* [Scroll Position](#markdown-header-scroll-position)

## Scroll Position
Creates a data-scroll attribute on the html element and populates it with the current scroll position. CSS can then use this data attribute to create effects based on the scroll position.

```css
html:not([data-scroll='0']) header {
  /* Add styles here to be applied when the user has scrolled down */

  background-color: rgba(255, 255, 255, .9); /* turns the header background slightly transparent on scroll. */
  
}
```

Uses a debounce function to improve performance. Credit and to [pqina.nl](https://pqina.nl/blog/applying-styles-based-on-the-user-scroll-position-with-smart-css/), where you can read a detailed description of how it works.

